# INTRODUCTION

This repository contains scripts to build and release Node binaries.

## System Requirements

See [Building Node](https://github.com/nodejs/node/blob/main/BUILDING.md)
for system requirements.

Additional requirements:

* Ubuntu:
  * `patchelf`

## Getting The Code

To clone the
[Node Build repository](https://gitlab.com/Distributed-Compute-Protocol/node-build),
enter the following:
```
git clone --recursive git@gitlab.com:Distributed-Compute-Protocol/node-build.git
```

## Building

To build, enter the following:
```
mkdir build
cd build
cmake ..
cmake --build .
```

## Deploying

To deploy, clone
[dcp-native-ci](https://gitlab.com/Distributed-Compute-Protocol/dcp-native-ci)
and read the "Deploying" section of the "README.md" file therein.
